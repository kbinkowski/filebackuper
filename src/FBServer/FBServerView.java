package FBServer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;



/**
 * Klasa reprezentujca okno gl�wne serwera usugi archiwizatora.
 * @author F-B team
 */
public class FBServerView extends JFrame{ 
	
	/**
	 * Pole reprezentuj�ce serwer aplikacji archiwizacji plik�w. 
	 */
	private FBServer server;
	
    /**
     * Pole s�u��ce do prezentacji informacji o stanie aplikacji.
     */
    private TextArea logArea;
    
	/**
	 * Konstruktor okna aplikacji serwera usugi archiwizatora.
	 */
	FBServerView(FBServer server){
		super("FileBackuper Server");
		this.server = server;
		setMinimumSize(new Dimension(600,300));
		init();
		pack();
		setVisible(true);
	}  
	
	/**
	 * Metoda inicjuj�ca okno aplikacji serwera, wo�ana z konstruktora klasy FBServerView.
	 */
	private void init(){
		logArea =new TextArea();
		logArea.setEditable(false);
		setLayout(new BorderLayout());
		add(logArea, BorderLayout.CENTER);
		addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
            System.exit(0);
            }
        }); 
	} 
	
	/**
	 * Metoda prezentuj�ca na konsoli informacj�.
	 */
	void printInfo(String info){
		logArea.append(info+"\n");
	}	
	
}
