package FBServer;

import FBClient.FBClientInterface;

public class FBService {
    /**
     * Serwer, w kt�rym reprezentowany jest klient.
     */
    private final FBServer server;
	
	/**
     * Reprezentowany klient.
     */
    private final FBClientInterface client;
    
	/**
     * Nazwa reprezentowanego klienta.
     */
    private final String name;
    
	/**
     * Scie�ka do pliku konfiguracyjnego u�ytkownika.
     */
    private final String path;
    
    
    /**
     * Utworzenie reprezentacji podanego klienta w podanym serwerze. 
     * @param server serwer, w kt�rym ma istnie� ta reprezentacja.
     * @param client reprezentowany klient.
     */
    FBService(FBServer server, FBClientInterface client, String name) {
    	//wczytanie pliku konfiguracyjnego, zawieraj�cego m.in. info jakie pliki archiwizujemy, ich wersj� i wynik hashu tak aby nie liczy� za ka�dym razem
    	//plik xmlowy
    	//gdy plik nie istnieje - wi�c tworzymy nowego u�ytkownika - to podczas wczytywania pliku konfiguracyjnego rzuca wyj�tek
    	this.name   = name;
    	this.path   = "./User/"+name+".xml";
        this.server = server;
        this.client = client;
    }
    
    /**
     * Metoda zapisuj�ca dane przechowuj�ce w serwisie do pliku konfiguracyjnego u�ytkownika.
     */
    void saveData(){
    	//to do
    }

}
