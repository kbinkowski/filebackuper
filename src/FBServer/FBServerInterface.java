package FBServer;

import java.rmi.Remote;
import java.rmi.RemoteException;

import FBClient.FBClientInterface;

public interface FBServerInterface extends Remote{
	//metody zdalne wo�ane przez u�ytkownika, podstawowy argument to nazwa po kt�rej w serwerze identyfikujemy u�ytkownika
	int  logOn(String name, String pass, FBClientInterface client) throws RemoteException;
	void logOff(String name) throws RemoteException;

}
