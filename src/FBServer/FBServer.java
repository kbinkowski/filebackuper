package FBServer;

import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import FBClient.FBClientInterface;

public class FBServer extends UnicastRemoteObject implements FBServerInterface{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pole reprezentuj�ce nazw� serwera po kt�rej jest identyfikowany (adres+port+us�uga) - odczytywana z pliku konfiguracyjnego. 
	 */
	String nameServer;
	/** 
	 *Pole reprezentuj�ce port na kt�rym uruchomiony jest serwer. Zawarty w nazwie serwera. 
	 */
	int portServer;
	
	/** 
	 *Pole reprezentuj�ce widok  serwera. 
	 */
	FBServerView serverView;
	
	/** 
	 *Klienci znajduj�cy si� w bazie serwera identyfikowani po nazwie i ha�le.  
	 */
	HashMap<String, String> clients;
	
	/** 
	 *Aktualnie zalogowani klienci identyfikowani po unikatowej nazwie, reprezentowani przez service jaki ich obs�uguje. 
	 */
	HashMap<String, FBService> actualClients;
	
	
	/**
	 *Konstruktor klienta usugi archiwizatora. 
	 */
	FBServer() throws RemoteException {
		//to do metoda wczytywania
		//odczytanie pliku konfiguracyjnego - najlepiej sporz�dzonego w xml
		//teraz zamiast odczytywa� ustawiam
		portServer = 1099;
		try{ 
			InetAddress addr = InetAddress.getByName("localhost");
			nameServer = "rmi://"+addr.getHostAddress()+":"+portServer+"/backuper"; 
		}catch (Exception e){
			System.err.println("Error creating ServerView: " + e);
			System.exit(1);
		}
		
        final FBServer me = this;
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try { 
					serverView = new FBServerView(me); 
					printInfo("Uruchomiony serwer o nazwie: "+getName());
				} catch (Exception e) {
					System.err.println("Error creating ServerView: " + e);
		            System.exit(1);
				}
	        }
	   });
		
	   clients = new HashMap<String,String>();
	   actualClients = new HashMap<String,FBService>();
	}
	
	//Metody interfejsu serwera
	public int logOn(String name, String pass, FBClientInterface client) throws RemoteException{
		int result; // -2 gdy u�ytkownik jest zalogowany na innym kompie, -1 gdy nieprawidowe haso, 1 gdy zalogowano lub stworzono i zalogowano u�ytkownika poprawnie
		synchronized (clients){
			if(clients.containsKey(name)){
				printInfo(clients.get(name));
				printInfo(pass);
				if (clients.get(name).equals(pass)){
					if (actualClients.containsKey(name)){
						result = -2;
					}
					else{
						result = 1;
						actualClients.put(name, new FBService(this, client, name)); // dzi�ki przekazaniu nazwy klienta, Serwis w konstruktorze wczyta plik konfiguracyjny u�ytkownika
					}
				}
				else
					result = -1;
			}
			else{
				clients.put(name, pass);
				actualClients.put(name, new FBService(this, client, name));
				result = 1;
			}
		}
		
		if (result == 1)
			printInfo("Zalogowano u�ytkownika: "+name+".");
		return result;
	}
	
	public void logOff(String name) throws RemoteException{
		synchronized (actualClients){
			FBService  service = actualClients.get(name);
			service.saveData();
			actualClients.remove(name);
		}
		printInfo("Wylogowano u�ytkownika: "+name+".");
	}
	
	/**
	 * Metoda zwracaj�ca nazw� serwera. 
	 */
	String getName(){
		return nameServer;
	}
	
	/**
	 * Metoda zwracaj�ca port na kt�rym uruchomiony jest serwer. 
	 */
	int getPort(){
		return portServer;
	}
	
	/**
	 * Metoda prezentuj�ca na konsoli informacj�.
	 */
	void printInfo(String info){
		serverView.printInfo(info);
	}
	
	/**
	 * G��wna metoda klasy.
	 * @param args
	 */
	public static void main(String args[]) {		
        try { System.setSecurityManager(new RMISecurityManager()); } 
        catch (SecurityException e) {
            System.err.println("Security violation " + e);
            System.exit(1);
        }
        
		try{ 
			FBServer theServer = new FBServer(); 
			LocateRegistry.createRegistry(theServer.getPort());
			Naming.rebind("backuper", theServer);
		} catch(Exception e){
			System.err.println("Error creating CreatingServer: " + e);
            System.exit(1);	
		}
		
	}
}
