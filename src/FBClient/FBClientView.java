package FBClient;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;


/**
 * Klasa reprezentujca okno gl�wne klienta usugi archiwizatora.
 * @author F-B team
 */
public class FBClientView extends JFrame implements ActionListener, TreeSelectionListener{
	/**
	 * Pole reprezentuj�ce aktualnie zalogowanego klienta. 
	 */
	FBClient client;
	
	/**
	 * Menager wyboru lokalnego pliku do archiwizacji.
	 */
	JFileChooser fileChooser;
	
	/**
	 * Pole wyboru serwera.
	 */
	private JTextField server;
	
	/**
	 * Pole nazwy u�ytkownika.
	 */
	private JTextField userName;
	
	/**
	 * Pole hasla u�ytkownika.
	 */
	private JPasswordField userPassword;
	
	/**
	 * Przycisk odpowiadaj�cy za polaczenie z serwerem. 
	 */
	private JButton connect;
	
	/**
	 * Przycisk odpowiadaj�cy za rozlaczenie z serwerem.
	 */
	private JButton disconnect;
	
	/**
	 * Drzewo lokalnych plik�w przeznaczonych do archiwizacji.
	 */
	private JTree localTree;
	
	/**
	 * Korze� drzewa lokalnych plik�w przeznaczonych do archiwizacji.
	 */
	private DefaultMutableTreeNode localTreeNode;
	
	/**
	 * Drzewo zdalnych plik�w w archiwum.
	 */
	private JTree remoteTree;
	
	/**
	 * Korze� drzewa zdalnych plik�w w archiwum.
	 */
	private DefaultMutableTreeNode remoteTreeNode;
	
	/**
	 * Przycisk odpowiadaj�cy za dodanie nowego pliku do archiwizacji.
	 */
	private JButton addLocalFile;
	
	/**
	 * Przycisk odpowiadaj�cy za usuniecie pliku do archiwizacji.
	 */
	private JButton removeLocalFile;
	
	/**
	 * Przycisk odpowiadaj�cy za rozpoczecie przesylania plik�w do archiwizacji.
	 */
	private JButton sendFile;	
	
	/**
	 * Przycisk odpowiadaj�cy za usuniecie pliku archiwizowanego.
	 */
	private JButton removeRemoteFile;
	
	/**
	 * Przycisk odpowiadaj�cy za rozpoczecie pobierania plik�w archiwizowanych.
	 */
	private JButton loadFile;
	
    /**
     * Pole s�u��ce do prezentacji informacji o stanie aplikacji.
     */
    private TextArea logArea;
    
    /**
     * Pole s�u��ce do prezentacji informacji o wyniku przesy�ania pliku.
     */
    private JProgressBar progressBar;
    
    

	/**
	 * Konstruktor okna aplikacji klienta usugi archiwizatora.
	 */
	FBClientView(FBClient client){
		super("FileBackuper");
		this.client = client;
		setMinimumSize(new Dimension(800,600));
		init();
		pack();
		setVisible(true);
		addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { System.exit(0); } }); 
	}  
	
	/**
	 * Metoda inicjuj�ca okno aplikacji klienta.
	 */
	private void init(){
		//Stworzenie komponent�w sk�adowych
		createComponent();
		
		//Glowne panele okna
		GridLayout gridLayout = new GridLayout(1,2);
		gridLayout.setHgap(100);
		gridLayout.setVgap(20);
		JPanel logging = new JPanel(gridLayout);
		logging.setBorder(BorderFactory.createTitledBorder("Logowanie"));
		JPanel backuper = new JPanel(new BorderLayout());
		backuper.setBorder(BorderFactory.createTitledBorder("Backuper"));
		JPanel logs = new JPanel(new BorderLayout());
		logs.setBorder(BorderFactory.createTitledBorder("Stan"));
        setLayout(new BorderLayout());
        add(logging, BorderLayout.NORTH);
        add(backuper, BorderLayout.CENTER);
        add(logs, BorderLayout.SOUTH);
        
        //Panel logowania
        JPanel infoPanel = new JPanel(new BorderLayout());
        JPanel commandInfoPanel = new JPanel(new GridLayout(3,1)); 
        JPanel textInfoPanel = new JPanel(new GridLayout(3,1)); 
        commandInfoPanel.add(new JLabel("    Server:    "));
        commandInfoPanel.add(new JLabel("    U�ytkownik:    "));
        commandInfoPanel.add(new JLabel("    Has�o:    "));
        textInfoPanel.add(server);
        textInfoPanel.add(userName);
        textInfoPanel.add(userPassword);
        infoPanel.add(commandInfoPanel, BorderLayout.WEST);
        infoPanel.add(textInfoPanel, BorderLayout.CENTER);
        JPanel infoButtonPanel = new JPanel(new GridLayout(2,1));
        infoButtonPanel.add(connect);
        infoButtonPanel.add(disconnect);          
        logging.add(infoPanel);
        logging.add(infoButtonPanel);
        
        //Panel statusu
        logs.add(logArea, BorderLayout.CENTER);
        logs.add(progressBar, BorderLayout.SOUTH);
        
        //Panel backupera
        GridLayout gridLayout2 = new GridLayout(1,8);
        gridLayout2.setHgap(10);
        JPanel buttonPanel = new JPanel(gridLayout2);
        buttonPanel.add(addLocalFile);
        buttonPanel.add(removeLocalFile);
        buttonPanel.add(new JLabel());
        buttonPanel.add(sendFile);
        buttonPanel.add(loadFile);
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(removeRemoteFile);
        backuper.add(buttonPanel, BorderLayout.SOUTH);
        JPanel treesPanel = new JPanel(new GridLayout(1,2));
        JScrollPane localTreePane = new JScrollPane(localTree);
        JScrollPane remoteTreePane = new JScrollPane(remoteTree);
        treesPanel.add(localTreePane);
        treesPanel.add(remoteTreePane);
        
        //W celu jedynie wy�wietlenia designu
        DefaultMutableTreeNode plik1 = new DefaultMutableTreeNode("Plik 1");
        remoteTreeNode.add(plik1);
        DefaultMutableTreeNode plik1v1 = new DefaultMutableTreeNode("wersja1_2013.11.13_23:28");
        plik1.add(plik1v1);
        DefaultMutableTreeNode plik1v2 = new DefaultMutableTreeNode("wersja2_2013.11.14_23:28");
        plik1.add(plik1v2);
        backuper.add(treesPanel, BorderLayout.CENTER);
        
       
	}  
	
	/**
	 * Metoda tworz�ca komonenty sk�adowe okna aplikacji klienta.
	 */
	private void createComponent(){
		 fileChooser = new JFileChooser();
		 server = new JTextField("rmi://127.0.0.1:1099/backuper");
	     userName = new JTextField();
	     userPassword = new JPasswordField();
	     connect = new JButton("Po��cz");
	     connect.addActionListener(this);
	     disconnect = new JButton("Roz��cz");
	     disconnect.addActionListener(this);
	     logArea =new TextArea();
	     logArea.setEditable(false);
	     progressBar = new JProgressBar();
	     localTreeNode = new DefaultMutableTreeNode("Komputer");
	     localTree = new JTree(localTreeNode);
	     remoteTreeNode = new DefaultMutableTreeNode("Archiwum");
	     remoteTree = new JTree(remoteTreeNode);
	     localTree.addTreeSelectionListener(this);
	     remoteTree.addTreeSelectionListener(this);
	     addLocalFile = new JButton("Dodaj");
	     addLocalFile.addActionListener(this);
	     removeLocalFile = new JButton("Usu�");
	     removeLocalFile.addActionListener(this);
	     sendFile = new JButton("Archiwizuj");
	     sendFile.addActionListener(this);
	     removeRemoteFile = new JButton("Usu�");
	     removeRemoteFile.addActionListener(this);
	     loadFile = new JButton("Pobierz");
	     loadFile.addActionListener(this);
	     
	     //Set enabled of component
	     updateEnabledButton();
	     updateEnabledLogging(true);
	     updateEnabledTree(false);
	     updateProgressBar(0);
	}
	
	
	/**
	 * Metoda prezentuj�ca na konsoli informacj�.
	 */
	void printInfo(String info){
		logArea.append(info+"\n");
	}
	
	/**
	 * Metoda aktualizuj�ca stan przycisk�w.
	 */
	void updateEnabledButton(){
		if(client.isLogOn()){
			connect.setEnabled(false);
			addLocalFile.setEnabled(true);
			disconnect.setEnabled(true);
		}
		else{
			connect.setEnabled(true);
			addLocalFile.setEnabled(false);
			disconnect.setEnabled(false);
		}
		
		if(localTree.isSelectionEmpty())
			this.removeLocalFile.setEnabled(false);
		else
			this.removeLocalFile.setEnabled(true);
		
		if(localTreeNode.isLeaf()) 
			this.sendFile.setEnabled(false);
		else 
			this.sendFile.setEnabled(true);
		
		if(remoteTreeNode.isLeaf())
			this.loadFile.setEnabled(false);
		else
			this.loadFile.setEnabled(true);
		
		if(remoteTree.isSelectionEmpty())
			this.removeRemoteFile.setEnabled(false);
		else
			this.removeRemoteFile.setEnabled(true);
	}
	
	/**
	 * Metoda ustawiaj�ca mo�liwo�� edytowania p�l identyfikuj�cych serwer i u�ytkownika.
	 */
	void updateEnabledLogging(boolean choice){
		server.setEnabled(choice);
		userName.setEnabled(choice);
		userPassword.setEnabled(choice);
	}
	
	/**
	 * Metoda ustawiaj�ca mo�liwo�� edycji drzew plik�w.
	 */
	void updateEnabledTree(boolean choice){
		localTree.setEnabled(choice);
		remoteTree.setEnabled(choice);
	}
	
	/**
	 * Metoda aktualizuj�ca stan pasku post�pu.
	 */
	void updateProgressBar(int i){
		progressBar.setValue(i);
	}
	
	/**
	 * Metoda aktualizuj�ca drzewo plik�w zarchiwizowanych.
	 */		
	void updateRemoteTree(HashMap<String, ArrayList<String> > remoteMap){ //nie sprawdza�em czy dzia�a
		Set set = remoteMap.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()){
			Map.Entry<String, ArrayList<String> > me = (Map.Entry<String, ArrayList<String> >) i.next();
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(me.getKey());
			remoteTreeNode.add(node);
			ArrayList<String> al = (ArrayList<String>) me.getValue();
			Iterator<String> s = al.iterator();
			while (s.hasNext()){
				DefaultMutableTreeNode child = new DefaultMutableTreeNode(s.next());
				node.add(child);
			}
		}
		remoteTree.clearSelection();
		remoteTree.updateUI();
	}
	
	/**
	 * Metoda aktualizuj�ca drzewo plik�w do archiwizocji.
	 */	
	void updateLocalTree(HashSet<String> localFiles){
		localTreeNode.removeAllChildren();
		Iterator<String> i = localFiles.iterator();
		while(i.hasNext()){
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(i.next());
			localTreeNode.add(node);
		}
		localTree.clearSelection();
		localTree.updateUI();
	}

	
	public void actionPerformed(ActionEvent e){
		if (e.getSource() == connect){
			
			try{
				String serv = server.getText();
				String name = userName.getText();
				String pass = userPassword.getText();
				if(name.equals("") || pass.equals("") || serv.equals("")){
					printInfo("Wype�nij wszystkie pola identyfikuj�ce u�ytkownika i serwer.");
				}
				else{
					if(client.logOn(serv, name, pass)){                   
						updateEnabledLogging(false);
						updateEnabledTree(true);
					} 
				}
			}catch (Exception p){
				logArea.append("B��d. Nieprawid�owe dane. \n");
			}
			
		}else if (e.getSource() == disconnect){
			
			 try{
    			 if (client.logOff()){
    				 updateEnabledLogging(true);
    				 updateEnabledTree(false);
    				 logArea.append("Roz��czono z serwerem.\n");
    			 } else
    				 logArea.append("B��d podczas roz��czania serwera. \n");
    		 }catch (Exception p){
    			 logArea.append("B��d podczas roz��czania serwera. \n");
    		 }
			 
		}else if (e.getSource() == addLocalFile){
			
   		 	try{
   		 		//Dodaje nowy plik do archiwizacji
   		 		int returnValue = fileChooser.showOpenDialog(this);
   		 		if (returnValue == JFileChooser.APPROVE_OPTION){
   		 			File file = fileChooser.getSelectedFile();
   		 			//przekazuje plik do modelu, kt�ry uaktualnia list� plik�w
   		 			if(client.addLocalFile(file.getPath())){
   		 				logArea.append("Dodano plik o nazwie: "+file.getName()+".\n");
   		 			}
   		 			else
   		 				logArea.append("Plik o podanej �cie�ce dodany ju� do listy plik�w archiwizowanych.\n");
   		 		} 
   		 	}catch (Exception p){
   		 		logArea.append("B��d podczas dodawania pliku.\n");
   		 	}
   		 	
		}else if (e.getSource() == removeLocalFile){
			
			HashSet<String> files = new HashSet<String>();
			int[] rows = localTree.getSelectionRows();
			if (rows[0]>0){
				for(int i=0; i<rows.length; ++i)
					files.add(localTreeNode.getChildAt(rows[i]-1).toString());
			}
			else {
				for(int i=0; i<localTreeNode.getChildCount(); ++i)
					files.add(localTreeNode.getChildAt(i).toString());
			}
			client.removeLocalFile(files);
			logArea.append("Usuni�to pliki z listy przeznaczonych do archiwizacji.\n");
			
		}else if (e.getSource() == sendFile){

			//archiwizujemy wszystkie pliki na li�cie
			client.sendFiles();
			
		}else if (e.getSource() == loadFile){
			
			//pobieramy tylko zaznaczone pliki
			/*ArrayList<String> files = new ArrayList<String>();
			int[] rows = remoteTree.getSelectionRows();
			if (rows.length>0){ //precyzujemy jakie pliki odtwarzamy
				if(rows[0] == 0){  //zaznaczony korze� wi�c odtwarzamy wszystkie w wersji najnowszej\
					int numberFiles = remoteTreeNode.getChildCount();
					for(int i=0; i<numberFiles; ++i){
						DefaultMutableTreeNode node = remoteTreeNode.get
						files.add(node.toString()+"/"+node.)
					}
				}
			} */
			
		}else if (e.getSource() == removeRemoteFile){
			//Uaktualnia list� w modelu
			logArea.append("Usuni�to plik z archiwum.\n");
		}
		
		updateEnabledButton();
	}
	
	 public void valueChanged(TreeSelectionEvent e) {
		 updateEnabledButton();
 	 }

}
