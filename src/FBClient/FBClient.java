package FBClient;
import java.io.File;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import FBServer.FBServerInterface;

/**
 * Klasa reprezentuj�ca klienta us�ugi archiwizacji plik�w.
 * @author F-B team
 */
public class FBClient implements FBClientInterface{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pole reprezentuj�ce widok  klienta. 
	 */
	FBClientView clientView;
	
	/**
	 * Referencja na zdalny obiekt serwera aplikacji archiwizacji plik�w.
	 */
	FBServerInterface server;
	
	/**
	 * Flaga informuj�ca czy klient jest zalogowany. 
	 */
	boolean logFlag;
	
	/**
	 * Nazwa klienta zalogowanego. 
	 */
	String name;	
	
	/**
	 * Kolekcja przechowuj�ca informacje o zarchiwizowanych plikach klienta. 
	 */
	HashMap<String, ArrayList<String> > remoteFiles;
	
	/**
	 * Kolekcja przechowuj�ca informacje o plikach klienta do zarchiwizowania. 
	 */
	HashSet<String> localFiles;
	
	/**
	 * Konstruktor klienta usugi archiwizatora.
	 */
	FBClient(){
		logFlag = false;
		//ewentualnie zmieni� na inne kolekcje lub rozwi�zanie z drzewami
		remoteFiles = new HashMap<String, ArrayList<String> >();
		localFiles  = new HashSet<String>();
		
        final FBClient me = this;
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try { clientView = new FBClientView(me); } 
				catch (Exception e) {
					System.err.println("Error creating ClientView: " + e);
		            System.exit(1);
				}
	        }
	    });
		
		try {
            UnicastRemoteObject.exportObject(this);
        } catch (Exception e) {
        	System.err.println("Error exporting client: " + e);
        	System.exit(1);
        }
	}
	
	/**
	 *Metoda informuj�ca czy klient jest zalogowany. U�ywana w widoku do aktualizacji stanu.
	 */
	boolean isLogOn(){
		return logFlag;
	}
	
	/**
	 * Metoda logowania uzytkownika do serwera. Zwraca czy zalogowanie przebieg�o poprawnie.
	 */	
	boolean logOn(String server, String userName, String userPass){ 
        Remote remoteObject = null;
        try { remoteObject = Naming.lookup(server); } 
        catch (Exception e) {
            printInfo("B��d. Nie rozpoznano serwera. " + e);
            return false;
        }

        if (remoteObject instanceof FBServerInterface)
            this.server = (FBServerInterface) remoteObject;
        else {
        	printInfo("B��d. Serwer o podanej nazwie nie pe�ni us�ugi archiwizacji plik�w. ");
            return false;
        }
        
        try{ 
        	switch(this.server.logOn(userName, userPass, this)){
        	case -2:
        		printInfo("U�ytkownik " + userName + " istnieje w bazie i jest aktualnie zalogowany na innym komputerze.");
        		return false;
        	case -1:
        		printInfo("U�ytkownik " + userName + " istnieje w bazie, podaj prawidowe haso.");
        		return false;
        	case 1:
        		name = userName;
        		printInfo("Poprawnie zalogowano u�ytkownika: "+userName+".");
        		logFlag = true;
        		return true;
        	default:
        		printInfo("B��d serwera.");
        		return false;
        	}        		
        }catch (Exception e){
        	printInfo("B��d serwera.");
        	logFlag = false;
            return false;
        }
   	}
	
	/**
	 * Metoda wylogowania uzytkownika z serwera.
	 */	
	boolean logOff(){
		try{
			this.server.logOff(name);
			logFlag = false;
			printInfo("Wylogowano.");
		} catch (Exception e){
			logFlag = true;
			printInfo("B��d serwera.");
		}
		return !logFlag;
	}
	
	/**
	 * Metoda dodaj�ca nowy plik przeznaczony do archiwizacji.
	 */	
	boolean addLocalFile(String f){
		boolean result;
		if(result=localFiles.add(f)) 
			clientView.updateLocalTree(localFiles);
		return result;
	}
	
	/**
	 * Metoda usuwaj�ca plik przeznaczony do archiwizacji.
	 */	
	void removeLocalFile(HashSet<String> f){
		Iterator<String> i = f.iterator();
		while(i.hasNext())
			localFiles.remove(i.next()); 
		clientView.updateLocalTree(localFiles);
	}
	
	/**
	 * Metoda usuwaj�ca plik przeznaczony do archiwizacji.
	 */	
	void sendFiles(){
		//Przesy�amy wszystkie pliki znajduj�ce si� w li�cie plik�w lokalnych
		ArrayList<File> files = new ArrayList<File>();
		Iterator<String> i = localFiles.iterator();
		while(i.hasNext())
			files.add(new File(i.next()));
		if(!files.isEmpty()){
		//to do
		//stworzyli�my now� tablic� plik�w i teraz je�li zmienimy list� zanim wszystkie pliki zostan� przes�ane to i tak polecenie przes�ania plik�w zostanie nie zmienione
		//tworzymy nowy w�tek kt�ry przesy�a pokolei pliki z tablicy, u�ywamy jakiego� strumieniu Bajtowego z ustawionym parametrem m�wi�cym o rozmiarze cz�ci na kt�re podzielony jest plik w celu przes�ania
		//Przesy�aj�c plik mamy dost�p do paska post�pu i konsoli, w odpowiednich momentach je usatwiamy
		//stworzenie nowego w�tku pozwoli nam przes�anie plik�w w tle, interfejs graficzny nadal b�dzie aktywny
		}
	}
	
	void loadFiles(ArrayList<String>files){
		//to do
		//dzi�ki temu �e na serwerze przechowujemy ca�� scie�k� pliku to odtwarzamy plik w folderze docelowym
	}
	
	/**
	 *Metoda prezentuj�ca na konsoli informacj�. 
	 */
	void printInfo(String info){
		clientView.printInfo(info);
	}

	/**
	 * G��wna metoda klasy.
	 * @param args
	 */
	public static void main(String args[]) {
        try {
            System.setSecurityManager(new RMISecurityManager());
        } catch (SecurityException e) {
            System.err.println("Security violation " + e);
            System.exit(1);
        }
		new FBClient();
	}
}
